#[doc = include_str!("../README.md")]
use std::{
    collections::{BTreeMap, BTreeSet},
    hash::Hasher,
};

#[derive(Debug)]
pub enum Error {
    KeyConflict,
    ValueConflict,
}

pub fn varid<'a>(idents: &[&'a str]) -> Result<BTreeMap<&'a str, u64>, Error> {
    let mut result = BTreeMap::new();
    let mut values = BTreeSet::new();
    for ident in idents {
        if result.contains_key(*ident) {
            return Err(Error::KeyConflict);
        }
        let val = conv(ident);
        if values.contains(&val) {
            return Err(Error::ValueConflict);
        }
        result.insert(*ident, val);
        values.insert(val);
    }
    Ok(result)
}

const fn mmask(n: u32) -> u64 {
    !(u64::MAX << (n * 7))
}

pub fn conv(s: &str) -> u64 {
    let s = s.as_bytes();
    let mut hasher = ahash::AHasher::new_with_keys(20220630, 1408);
    hasher.write(s);
    let val = hasher.finish();
    let mut mask = mmask(1);
    if (s.len() >= 3) && s.starts_with(b"sz") {
        mask = match s[2] {
            b'2' => mmask(2),
            b'3' => mmask(3),
            b'4' => mmask(4),
            b'5' => mmask(5),
            b'6' => mmask(6),
            b'7' => mmask(7),
            b'8' => mmask(8),
            b'9' => mmask(9),
            _ => mmask(1),
        };
    };
    val & mask
}

#[inline]
pub fn check(s: &str, n: u64) -> bool {
    conv(s) == n
}

#[test]
fn test() {
    let x = varid(&["a", "e", "i", "o", "u", "uu", "dd", "sz9"]).unwrap();
    assert_eq!(x["a"], 36);
    assert_eq!(x["e"], 70);
    assert_eq!(x["i"], 11);
    assert_eq!(x["o"], 20);
    assert_eq!(x["u"], 22);
    assert_eq!(x["uu"], 103);
    assert_eq!(x["sz9"], 4289387282599257848);
    assert_eq!(x["dd"], 30);
}
