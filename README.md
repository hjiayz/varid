# varid

## convert ident string to hash value, and check conflict

#### ident prefix sz2 14 bit
#### ident prefix sz3 21 bit
#### ident prefix sz4 28 bit
#### ident prefix sz5 35 bit
#### ident prefix sz6 42 bit
#### ident prefix sz7 49 bit
#### ident prefix sz8 56 bit
#### ident prefix sz9 63 bit
#### other 7 bit

```rust
    use varid::{varid,check,conv};
    let map=varid(&["a","e","sz1apple","sz2apple","sz3apple"]).unwrap();
    assert!(check("a",map["a"]));
    assert!(check("e",map["e"]));
    assert!(check("sz2apple",map["sz2apple"]));
    assert!(check("sz3apple",map["sz3apple"]));
    assert_eq!(conv("a"),36);
    assert_eq!(conv("e"),70);
    assert_eq!(conv("sz2apple"),12857);
    assert_eq!(conv("sz3apple"),1755997);
    assert_eq!(conv("sz9apple"),4595417591438920017);
```